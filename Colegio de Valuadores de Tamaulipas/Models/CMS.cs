﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Collections;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ColegioValuadoresTamaulipas.Models
{

    //Entidad de Circular/Noticia
    public class BaseCircular
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Autor
        {
            get;
            set;
        }

        [Required]
        [Display(Name = "Fecha de creación")]
        public DateTime FechaCreacion { get; set; }

        [Required]
        public string Titulo { get; set; }

        [Required]
        [AllowHtml]
        public string Contenido { get; set; }
    }


    [Table("Circulares")]
    public class Circular : BaseCircular
    {
    }

    
    [Table("Noticias")]
    public class Noticia : BaseCircular
    {
    }


    [Table("Inicio")]
    public class Inicio
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Titulo { get; set; }

        [Required]
        [AllowHtml]
        public string Mensaje { get; set; }
    }

    
    /// <summary>
    /// Modelo para la forma de contacto.
    /// </summary>
    public class FormaContacto
    {
        [Required]
        [MaxLength(1024)]
        public string Nombre { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MaxLength(1024)]
        public string Asunto { get; set; }

        [Required]
        public string Mensaje { get; set; }
    }

    

    [Table("Contacto")]
    public class Contacto
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [AllowHtml]
        public string Mensaje { get; set; }

        [Required]
        public string Direccion { get; set; }

        [Required]
        public string Telefono { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    [Table("Acerca")]
    public class Acerca
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [AllowHtml]
        public string Contenido { get; set; }
    }


    public class Servicio
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Titulo { get; set; }

        [Required]
        [AllowHtml]
        public string Descripcion { get; set; }
    }


    public class Integrante
    {
        [Key]
        public int Id {get; set;}

        [Required]
        public string Nombre {get; set;}

        [AllowHtml]
        [Display(Name="Ficha Técnica")]
        public string FichaTecnica {get; set;}

        public string Telefono {get; set;}

        [EmailAddress]
        public string Email {get; set;}

    }


    /// <summary>
    /// Contexto de base datos principal para el sitio web
    /// </summary>
    public class CMSDbContext : DbContext
    {
        public DbSet<Circular> Circulares { get; set; }
        public DbSet<Noticia> Noticias { get; set; }

        public DbSet<Inicio> Inicio { get; set; }
        public DbSet<Acerca> Acerca { get; set; }
        public DbSet<Servicio> Servicios { get; set; }
        public DbSet<Integrante> Integrantes { get; set; }
        public DbSet<Contacto> Contacto { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public CMSDbContext() : base("Conexion") { }
    }
  
}
