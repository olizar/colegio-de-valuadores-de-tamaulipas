﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ColegioValuadoresTamaulipas.Startup))]
namespace ColegioValuadoresTamaulipas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
