﻿/*
* Autor: Ricardo Olivares Zarazúa
* 
* Script para el calculo de Vida Util Remanente (VUR)
* 
* 11/2014
*/

"use strict";

//Tipos de edificaciones
var edificaciones = {
    "1" :"Vivienda mínima de interés social (43 M2)",
    "2" : "Vivienda de interés social (80 M2)",
    "3" : "Vivienda de primera (250 M2)",
    "4" : "Vivienda de lujo (1000 M2)",
    "5" : "Edificio de departamentos (1,300 M2)",
    "6" : "Edificio comercial (9000 M2)",
    "7" : "Bodega (1,600 M2)",
    "8" : "Nave industrial (2,100 M2)"
};


//Porcentajes de intervencion por partida
var porcentajesIntervencion = {
    "1" : [ .468, .161, .279, .051, .041 ],
    "2" : [ .410, .11, .40, .07, .01 ],
    "3" : [ .369, .108, .427, .083, .013 ],
    "4" : [ .471, .095, .296, .088, .05 ],
    "5" : [ .452, .150, .330, .046, .022 ],
    "6" : [ .630, .085, .176, .076, .033 ],
    "7" : [ .790, .03, .156, .014, .01 ],
    "8" : [ .806, .025, .145, .014, .01 ]
};


//Edades ponderadas por partida
var edadesPonderadasPorPartida = {

    "1" : [ 70, 23.5, 14.7, 22.0, 10 ],
    "2" : [ 80, 25, 26.4, 34, 12 ],
    "3" : [ 140, 38, 33, 55, 54 ],
    "4" : [ 140, 42.5, 34, 56.8, 68.4  ],
    "5" : [ 160, 52.4, 36.1, 53, 56.1  ],
    "6" : [ 200, 47.5, 46.2, 76.5, 93.0 ],
    "7" : [ 90, 42.5, 28.6, 34.5, 32.1 ],
    "8" : [  150, 40.8, 28.1, 38.9, 36.4 ]
};


//VUT para cada tipo de edificación
var vidaUtilTotalPorEdificacion = {
    "1": 70,  //A1
    "2": 70,  //A2
    "3": 90,  //A4
    "4": 100, //A5
    "5": 60,  //B1
    "6": 40,  //C1
    "7": 50,  //C2
    "8": 60   //C3
};


var edadCronologica = 0;


/*
* Funcion setup para generar los options de los select para calificar la depreciacion por partida.
*/
function getOptionsForSelectRemodelacion()
{
    var estadosConservacion = new Array();

    estadosConservacion.push({ "factor": 1.0, "label": "Nuevo" });
    estadosConservacion.push({ "factor": .90, "label": "Bueno" });
    estadosConservacion.push({ "factor": .80, "label": "Medio" });
    estadosConservacion.push({ "factor": .70, "label": "Regular" });
    estadosConservacion.push({ "factor": .60, "label": "Reparaciones sencillas" });

    $(".partida select.edo-conservacion").each(function (indexInArray, valueOfElement) {
        for (var i = 0; i < estadosConservacion.length; i++)
            $(this).append("<option value=\"" + estadosConservacion[i].factor + "\">" + estadosConservacion[i].label + "</option>");
    });

}


/*
* Actualiza los valores de edad ponderada por partida y 
* porcentajes de intervencion al cambiar el tipo de edificación
*/
function actualizarValoresTipoEdificacion( arg )
{
    //Para cada partida, actualizar los valores de edad ponderada y porcentaje de intervención

    //Tenemos que saber que tipo de edificacion es
    var edificacionSeleccionada = $(this).find(":selected").val();
    
    //Edades ponderadas por partida
    var partidaIndex = 0; // 1 - 5
    $(".partida .edad-ponderada-partida").each(function (index, element) {
        $(element).text( edadesPonderadasPorPartida[ edificacionSeleccionada ][ partidaIndex++ ] );
    });


    //Porcentajes de intervención por partida
    partidaIndex = 0;
    $(".partida .porcentaje-intervencion").each(function (index, element) {
        $(element).text(porcentajesIntervencion[edificacionSeleccionada][partidaIndex++]);
    });


    //VUT y Demerito por edad
    var edadCronologica =  parseInt( $("#edad-cronologica").find(":selected").val() );
    var vutEdificacion = parseInt( vidaUtilTotalPorEdificacion[ edificacionSeleccionada ] );


    //Actualizar el VUT
    $("#vut").text(vidaUtilTotalPorEdificacion[edificacionSeleccionada]);

    $("#demerito-por-edad").text( edadCronologica / vutEdificacion  );
}



/**
*
* @param n Edad cronologica
* @param N Edad ponderada de la partida o Vida Util Total
* @param factorDeCondicion Factor de condicion segun apreciación del valuador [0.0, 1.0]
* @return Factor de depreciacion de la partida [0.0, 1.0]
*/
function metodoRossAndHeidecke( n, N, factorDeConservacion )
{
    return ( 1 - Math.pow( n / N, 1.4 ) ) * factorDeConservacion;
}



function calculoEdosConservacion( arg0 )
{
    
    var tipoEdificacion = $("#tipo-edificacion :selected").val();

    //Obtener factor seleccionado por el usuario
    var factor = $(this).val();
    var edadCronologica = $("#edad-cronologica").val();

    //Que partida es?
    var numPartida = $( this ).data("num-partida");
    var edadPonderadaPartida = edadesPonderadasPorPartida[ tipoEdificacion ][ numPartida - 1 ];


    //Porcentaje de intervencion de la partida
    var porcentajeIntervencionPartida = porcentajesIntervencion[ tipoEdificacion ][  numPartida - 1 ];

    //Llamar al metodo ross&heidecke
    var factorDepreciacion = metodoRossAndHeidecke(edadCronologica, edadPonderadaPartida, factor);

    $("tr#partida" + numPartida + " td.factor-depreciacion").text(factorDepreciacion);

    //Calcular VUR
    var vruRemodelado = factorDepreciacion * edadPonderadaPartida * porcentajeIntervencionPartida;
    var vruNoRemodelado = (1 - factorDepreciacion) * (edadPonderadaPartida - edadCronologica) * porcentajeIntervencionPartida ;

    $("tr#partida" + numPartida + " td.vur-partida").text( vruRemodelado + vruNoRemodelado );

    sumaVUR();

    //Calculo de edad efectiva
    // VUT - VUR
    $("#edad-efectiva").text(  vidaUtilTotalPorEdificacion[tipoEdificacion]  - ( vruRemodelado + vruNoRemodelado) );

}


function sumaVUR()
{
    var sigma = 0;

    $(".vur-partida").each(function (index, element) {
        
        if ( $(this).text() != "" )
        {
            sigma += parseInt($(this).text());
        }

    });

    $("#vur").text("");
    $("#vur").text( sigma  + " años" );
}


$(document).ready(function () {

    //Cargar tipos de edificaciones en select tipo-edificacion
    $.each(edificaciones, function (indexInArray, valueOfElement) {
        $("#tipo-edificacion").append("<option value=\"" + indexInArray + "\">" + valueOfElement + "</option>");
    });


    //Construir select de edad cronologica. Valores desde 1 - 100;
    for(var i = 1 ; i < 81; i++)
        $("#edad-cronologica").append( "<option value=\"" + i + "\">" + i + " años</option>" );


    getOptionsForSelectRemodelacion();


    $("select#tipo-edificacion").on("change", actualizarValoresTipoEdificacion);

    $("select.edo-conservacion").each(function (index, element) {
        $(element).on("change", calculoEdosConservacion);
    });
});