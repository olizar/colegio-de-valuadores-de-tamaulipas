﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ColegioValuadoresTamaulipas.Models;

namespace ColegioValuadoresTamaulipas.Controllers
{
    [Authorize]
    public class AppController : Controller
    {
        // GET: App
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Lanza la aplicacion web de calculo de Vida Util Remanente
        /// 
        /// La aplicación es una aplicación web enteramente en JavaScript y HTML5
        /// </summary>
        /// <returns></returns>
        public ActionResult CalculoVUR()
        {
            return View();
        }
    }

}
