﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ColegioValuadoresTamaulipas.Models;

namespace ColegioValuadoresTamaulipas.Controllers.CMS
{
    [Authorize]
    public class PortadaController : Controller
    {

        CMSDbContext db = new CMSDbContext();

        // GET: Portada
        public ActionResult Index()
        {
            return View( db.Inicio.First() );
        }

        [HttpPost]
        public ActionResult Guardar( Inicio inicio )
        {
            if( ModelState.IsValid )
            {
                db.Entry(inicio).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", "Sitio");
            }

            ModelState.AddModelError("", "Ocurrió un error al guardar los valores.");
            return View();

        }
    }
}