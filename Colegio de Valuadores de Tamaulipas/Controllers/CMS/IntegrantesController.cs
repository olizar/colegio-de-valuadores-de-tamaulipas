﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ColegioValuadoresTamaulipas.Models;

namespace ColegioValuadoresTamaulipas.Controllers.CMS
{
    [Authorize]
    public class IntegrantesController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        // GET: Integrantes
        public ActionResult Index()
        {
            return View(db.Integrantes.ToList());
        }

        // GET: Integrantes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Integrante integrantes = db.Integrantes.Find(id);
            if (integrantes == null)
            {
                return HttpNotFound();
            }
            return View(integrantes);
        }

        // GET: Integrantes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Integrantes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,Telefono,Email")] Integrante integrantes)
        {
            if (ModelState.IsValid)
            {
                db.Integrantes.Add(integrantes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(integrantes);
        }

        // GET: Integrantes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Integrante integrantes = db.Integrantes.Find(id);
            if (integrantes == null)
            {
                return HttpNotFound();
            }
            return View(integrantes);
        }

        // POST: Integrantes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,Telefono,Email")] Integrante integrantes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(integrantes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Sitio");
            }
            return View(integrantes);
        }

        // GET: Integrantes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Integrante integrantes = db.Integrantes.Find(id);
            if (integrantes == null)
            {
                return HttpNotFound();
            }
            return View(integrantes);
        }

        // POST: Integrantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Integrante integrantes = db.Integrantes.Find(id);
            db.Integrantes.Remove(integrantes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
