﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ColegioValuadoresTamaulipas.Models;

namespace ColegioValuadoresTamaulipas.Controllers.CMS
{
    [Authorize]
    public class SitioController : Controller
    {
        // GET: Sitio
        public ActionResult Index()
        {
            return View();
        }
    }
}