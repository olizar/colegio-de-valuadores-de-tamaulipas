﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ColegioValuadoresTamaulipas.Models;

namespace ColegioValuadoresTamaulipas.Controllers.CMS
{
    public class CircularesController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        // GET: Circulares
        public ActionResult Index()
        {
            return View(db.Circulares.ToList());
        }

        // GET: Circulares/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Circular circular = db.Circulares.Find(id);
            if (circular == null)
            {
                return HttpNotFound();
            }
            return View(circular);
        }

        // GET: Circulares/Create
        [Authorize(Users="admin@covaet.mx")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Circulares/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "admin@covaet.mx")]
        public ActionResult Create([Bind(Include = "Id,Autor,Titulo,Contenido")] Circular circular)
        {
            if (ModelState.IsValid)
            {
                circular.FechaCreacion = DateTime.Now;

                db.Circulares.Add(circular);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(circular);
        }

        // GET: Circulares/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Circular circular = db.Circulares.Find(id);
            if (circular == null)
            {
                return HttpNotFound();
            }
            return View(circular);
        }

        // POST: Circulares/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Autor,FechaCreacion,Titulo,Contenido")] Circular circular)
        {
            if (ModelState.IsValid)
            {
                db.Entry(circular).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(circular);
        }

        // GET: Circulares/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Circular circular = db.Circulares.Find(id);
            if (circular == null)
            {
                return HttpNotFound();
            }
            return View(circular);
        }

        // POST: Circulares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Circular circular = db.Circulares.Find(id);
            db.Circulares.Remove(circular);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
