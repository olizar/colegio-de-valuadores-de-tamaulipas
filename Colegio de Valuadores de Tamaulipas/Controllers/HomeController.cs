﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Net.Mail;

using ColegioValuadoresTamaulipas.Models;

namespace ColegioValuadoresTamaulipas.Controllers
{

    public class HomeController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        public ActionResult Index()
        {
            return View( db.Inicio.First() );
        }

        public ActionResult Acerca()
        {
            return View( db.Acerca.First() );
        }


        public ActionResult Servicios()
        {
            return View( db.Servicios.ToList() );
        }


        public ActionResult Integrantes()
        {
            return View( db.Integrantes.ToList() );
        }

        
        public ActionResult Noticias()
        {
            return View( db.Noticias.ToList() );
        }

        //GET /Contacto
        public ActionResult Contacto()
        {
            var contacto = db.Contacto.First();

            ViewBag.Direccion = contacto.Direccion;
            ViewBag.Email = contacto.Email;
            ViewBag.Telefono = contacto.Telefono;
            ViewBag.Mensaje = contacto.Mensaje;

            return View();
        }


        /// <summary>
        /// Envia una forma de contacto
        /// </summary>
        /// <param name="forma"></param>
        /// <returns></returns>
        
        [HttpPost]
        public ActionResult Contacto( FormaContacto forma  )
        {
            if( ModelState.IsValid )
            {
                try
                {
                    //Enviar email
                    SmtpClient cliente = new SmtpClient();
                    cliente.Host = "smtp-mail.outlook.com";
                    cliente.Port = 587;
                    cliente.Credentials = new System.Net.NetworkCredential("ricardo.olivares@outlook.com", "225923.outlook");
                    cliente.EnableSsl = true;


                    MailAddress to = new MailAddress("ricardo.olizar@gmail.com");
                    MailAddress from = new MailAddress("ricardo.olivares@outlook.com");

                    MailMessage mensaje = new MailMessage(from, to);

                    mensaje.Subject = "Nueva forma de contacto - covaet.mx";
                    string str = String.Format("Nombre: {0}\n Email: {1}\n Asunto: {2}\n Mensaje: {3}\n", forma.Nombre, forma.Email, forma.Asunto, forma.Mensaje);

                    mensaje.Body = "Prueba de mensaje";

                    ViewBag.Contacto_OK = true;
                    cliente.Send(mensaje);
                }
                catch (Exception ex)
                {
                    ViewBag.Contacto_OK = false;
                }
            }
            else
            {
                return View();
            }

            return View("AgradecimientosContacto");
        }
    }
}