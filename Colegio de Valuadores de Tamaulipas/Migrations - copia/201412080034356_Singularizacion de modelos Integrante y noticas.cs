namespace ColegioValuadoresTamaulipas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SingularizaciondemodelosIntegranteynoticas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Integrantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        FichaTecnica = c.String(),
                        Telefono = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Integrantes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Integrantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Descripcion = c.String(),
                        Telefono = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Integrantes");
        }
    }
}
