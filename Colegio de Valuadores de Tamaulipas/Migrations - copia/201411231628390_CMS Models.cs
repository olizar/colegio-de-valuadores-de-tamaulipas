namespace ColegioValuadoresTamaulipas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CMSModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Acercas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Contenido = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Inicios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false),
                        Mensaje = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Integrantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Descripcion = c.String(),
                        Telefono = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Servicios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false),
                        Descripcion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Servicios");
            DropTable("dbo.Integrantes");
            DropTable("dbo.Inicios");
            DropTable("dbo.Acercas");
        }
    }
}
