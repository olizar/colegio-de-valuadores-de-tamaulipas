namespace ColegioValuadoresTamaulipas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrimeraVersion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Circulars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Autor = c.String(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        Titulo = c.String(nullable: false),
                        Contenido = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Noticias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Autor = c.String(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        Titulo = c.String(nullable: false),
                        Contenido = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Noticias");
            DropTable("dbo.Circulars");
        }
    }
}
