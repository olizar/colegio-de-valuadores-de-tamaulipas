namespace ColegioValuadoresTamaulipas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Añadiduradecontactoalmodelo : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Acercas", newName: "Acerca");
            RenameTable(name: "dbo.Inicios", newName: "Inicio");
            CreateTable(
                "dbo.Contacto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Mensaje = c.String(nullable: false),
                        Direccion = c.String(nullable: false),
                        Telefono = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Contacto");
            RenameTable(name: "dbo.Inicio", newName: "Inicios");
            RenameTable(name: "dbo.Acerca", newName: "Acercas");
        }
    }
}
