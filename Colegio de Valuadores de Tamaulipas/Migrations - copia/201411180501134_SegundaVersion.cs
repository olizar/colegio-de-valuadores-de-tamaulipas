namespace ColegioValuadoresTamaulipas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SegundaVersion : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Circulars", newName: "Circulares");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Circulares", newName: "Circulars");
        }
    }
}
