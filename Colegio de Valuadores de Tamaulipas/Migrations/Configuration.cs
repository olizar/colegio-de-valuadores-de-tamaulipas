namespace ColegioValuadoresTamaulipas.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    using ColegioValuadoresTamaulipas.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ColegioValuadoresTamaulipas.Models.CMSDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ColegioValuadoresTamaulipas.Models.CMSDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //Seed de la base de datos.

            //Para la vista de contacto
            Contacto contacto = new Contacto
            {
                Id = 1,
                Direccion = "Calle Antonio Cardenas No. 303, Col. Nuevo Aeropuerto, Tampico, Tam.",
                Telefono = "01 833 228 0966",
                Email = "contacto@covet.mx",
                Mensaje = "Cualquier duda o comentario nos gustar�a nos lo hiciera llegar por el siguiente formulario de contacto. A la brevedad nos pondremos en contacto con usted."
            };

            context.Contacto.AddOrUpdate(contacto);

            //Para la vista de acerca
            Acerca acerca = new Acerca
            {
                Id = 1,
                Contenido = "Establezca el contenido de la secci�n �Acerca� en la secci�n �Sitio�."
            };

            context.Acerca.AddOrUpdate(acerca);

            //Para la portada
            Inicio portada = new Inicio
            {
                Id = 1,
                Titulo = "Configure el titulo de la portada",
                Mensaje = "Configure el mensaje de la portada"
            };

            context.Inicio.AddOrUpdate(portada);
        }
    }
}
