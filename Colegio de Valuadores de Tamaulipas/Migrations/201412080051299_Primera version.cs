namespace ColegioValuadoresTamaulipas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Primeraversion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Acerca",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Contenido = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Circulares",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Autor = c.String(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        Titulo = c.String(nullable: false),
                        Contenido = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contacto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Mensaje = c.String(nullable: false),
                        Direccion = c.String(nullable: false),
                        Telefono = c.String(nullable: false),
                        Email = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Inicio",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false),
                        Mensaje = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Integrantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        FichaTecnica = c.String(),
                        Telefono = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Noticias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Autor = c.String(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        Titulo = c.String(nullable: false),
                        Contenido = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Servicios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false),
                        Descripcion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Servicios");
            DropTable("dbo.Noticias");
            DropTable("dbo.Integrantes");
            DropTable("dbo.Inicio");
            DropTable("dbo.Contacto");
            DropTable("dbo.Circulares");
            DropTable("dbo.Acerca");
        }
    }
}
